#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#include "config.h"

/*
 *
 */
char io_read_char (void) {

    char c;

    do {
        c = getchar(); 
    } while (isspace(c));

    return c;
}

/*
 *
 */
int io_read_string(char * line, size_t len) {

    ssize_t read;

    // Show the prompt
    puts("server > "); 
    fflush(stdout);
    // Read a line
    if ( -1 == (read = getline(&line, &len, stdin))) {
        fprintf(stderr, "[#] Error on getline\n");
        return -1;   
    } else {
        if (line[read - 1] == '\n')  {
            line[read - 1] = '\0';
            --read;
        }
    }

    return 0;
}

/*
 *
 */
int io_check_user_answer_str(void) {

    char chose = io_read_char();

    while(NULL == strchr("ynYN", chose ) ? 1 : 0) {
        fprintf(stderr, "[#] Error on read_char(); %s\n", strerror(EINVAL));
        printf("Please, digit: [y] or [n], is not case sensitive: ");
        chose = io_read_char();
    } 

    if ( chose == 'Y' || chose == 'y') 
        return 0;
    else 
        return -1;
}

/*
 * Writes the header into file
 *
 * In the header is reported the UNIX timestamp and it's similar to:
 * ********************************************************
 * *  Chat Server started @ Sun Mar 6 15:55:39 CET 2011  **
 * ********************************************************
 *
 * Return value:
 *  0 on success
 * -1 on failure
 */
int io_write_header_t (char * p_path) {

    FILE * fp = NULL;
    char * time, * s;

    // checks if the path is a file, a dir or not exist
    tools_path_checker(p_path);

    if (NULL == (fp = fopen(p_path, "a"))) {
        fprintf(stderr, "I couldn't open %s\n", p_path);
        perror("The following error occurred ");
        fprintf(stderr, "Value of errno: %d\n", errno);
        return -1;
    } else {
        time = malloc(128 * sizeof(*time));
        s = malloc(61 * sizeof(*s)); memset(s, '*', 60);
        // Get the timestamp in UNIX format
        set_timestamp(time, UNIX_F);
        // Print in p_path file
        fprintf(fp, "%s\n**  Chat Server started  @ %s  **\n%s\n", s, time, s);
        fflush(fp); fclose(fp); free(time); free(s);
    }
    return 0;
}

/*
 * Writes a command to a file
 *
 * Writes the command into log-file. The access to log-file must be done in 
 * mutual exclusion to avoid mixing the prints with other thread.
 *
 * Note: Generally used by worker_thread() in chat-server.c
 *
 * Return value:
 *  0 on success
 * -1 on failure
 */
int io_write_cmd_t (char * p_path, char * cmd) {
	
    char * time;
    FILE * fp = NULL;
    pthread_mutex_t mux;
    pthread_mutex_init(&mux, NULL);

    // checks if the path is a file, a dir or not exist
    tools_path_checker(p_path);

    pthread_mutex_lock(&mux);
    if (NULL == (fp = fopen(p_path, "a"))) {
            fprintf(stderr, "I couldn't open %s\n", p_path);
            perror("The following error occurred ");
            fprintf(stderr, "Value of errno: %d\n", errno);
            return -1;
    } else {
        time = malloc(128 * sizeof(*time));
        // Get the time in TIME format
        set_timestamp(time, TIME_F);
        // Print in p_path file
        fprintf(fp, "%s: %s\n", time, cmd);
        fflush(fp); fclose(fp); free(time);
    }
    pthread_mutex_unlock(&mux);
    pthread_mutex_destroy(&mux);
        return 0;
}

/*
 * Writes a data_t (excluded the sockid) into file
 *
 * Note: Generally used by get_registration() in request.c
 *
 * Return value:
 *  0 on success
 * -1 on failure
 */
int io_write_data_t (char * p_path, data_t * p) {
    
    #ifdef DEBUG
        DEBUG_PRINT("%s\n","io_write_data_t() reached");
    #endif

    FILE * fp = NULL;
	
    if (NULL == (fp = fopen(p_path, "a"))) {
        fprintf(stderr, "I couldn't open %s\n", p_path);
        perror("The following error occurred ");
        fprintf(stderr, "Value of errno: %d\n", errno );
        return -1;	
    } else {
        fprintf(fp,"%s:%s:%s\n", p->uname, p->name, p->email);
        fflush(fp); fclose(fp);
    }
    return 0;
}

/*
 * Reads a data_t (excluded the sockid) from file
 */
int io_read_data_t(char * p_path) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","io_read_data_t() reached");
    #endif

    FILE * fp = NULL;
    data_t data;
    char * p_buff = calloc (3 * 256, sizeof(*p_buff));

    if(p_buff == NULL) {
        fprintf(stderr, "%s", strerror(ENOBUFS));
        exit(EXIT_FAILURE);
    }	

    if (tools_path_checker(p_path)) 
        return -1;

    if ((fp = fopen(p_path, "r")) == NULL) {
        fprintf(stderr, "I couldn't open %s\n", p_path);
        perror("The following error occurred ");
        fprintf(stderr, "Value of errno: %d\n", errno );
        return -1;
    } else {
        while(NULL != fgets(p_buff, 3 * 256, fp)) {
            if(feof(fp)) {
                printf("server: User configuration file read successfully!\n");
                break;
            } else {
                // tokenize the string into user struct
                strcpy(data.uname, strtok(p_buff, ":"));
                strcpy(data.name, strtok(NULL, ":"));
                strcpy(data.email, strtok(NULL, "\0"));
                // create a new node with the user struct
                //if (insert_list(p_list, data)) 
                if( insert_hash( Table, data, data.uname)) 
                        exit(EXIT_FAILURE);
                //printf("%s:%s:%s\n",data.uname,data.name,data.email);
                /* qta_lines is the number of lines read correctly. Is also 
                the number of nodes of the list. qta_lines == list_size */
            }
        }
    } 

    fclose(fp); fp = NULL; 
    return 0;
}

/*
 *
 
size_t io_read (int fd, void * buff, size_t count) {
    
    #ifdef DEBUG
        DEBUG_PRINT("%s\n","io_read() reached");
    #endif
    
    size_t result;
    size_t bytes_read = 0;
    
    while (bytes_read < count) {
        result = read( fd, buff, count - bytes_read);
        if ( -1 == result ) {
            // if interrupted by system call, repeat the loop
            if ( errno == EINTR )
                continue;
            else {
                perror("[#]  Error on read");
                close(fd); return result;
            }
        // end of file
        } else if (0 == result)
            break;
            
        bytes_read += result;
        buff += bytes_read;
    }
    
    return 0;
}


size_t io_write (int fd, const void * buff, size_t count) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","io_write() reached");
    #endif
    
    size_t bytes_write = 0;
    size_t result = -1;
    
    while (bytes_write < count) {
        result = write( fd, buff, count - bytes_write);
        if ( -1 == result ) {
            // if interrupted by system call, repeat the loop
            if ( errno == EINTR )
                continue;
            else {
                perror("[#]  Error on write");
                close(fd); return result;
            }
        // end of file
        } else if (0 == result)
            break;
        
        bytes_write += result;
    }
    
    return 0;
} */
