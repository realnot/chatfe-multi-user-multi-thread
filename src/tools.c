#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <regex.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

// User libraries
#include "config.h"

// Globals
#define M 997 

int _is_file_ = -1;
int _is_directory_ = -1;

/*
int _is_char_device_ = -1;
int _is_block_device_ = -1;
int _is_fifo_ = -1;
int _is_symlink_ = -1;
int _is_socket_ = -1;
*/	

/*
 *
 */
void set_timestamp(char * buf, size_t time_f) {

    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime(&rawtime);
    switch(time_f) {
        case UNIX_F: strftime(buf,128,"%a %b %d %X %Z %Y", timeinfo); break;
        case DATE_F: strftime(buf,128,"%x", timeinfo); break;
        case TIME_F: strftime(buf,128,"%X", timeinfo); break;
        case HUMS_F: strftime(buf,128,"%x - %X", timeinfo); break;
        default: exit(EXIT_FAILURE);
    }
}


/*
 * checking file info (with POSIX macros)
 * 
 * Return Values: the function return 0 when the file is a regular file and
 * return -1 in all other cases.
 */
int is_file(char * p_path) {
	
    struct stat * buf = malloc(sizeof(struct stat));

    if( ! stat(p_path, buf)) {
        if (S_ISREG(buf->st_mode)) {
            free(buf);
            return _is_file_ = 0;
        }
    } else
        fprintf(stderr, "server: %s, %s\n", p_path, strerror(errno));

    free(buf); 
    return -1;
}

/*
 * checking directory info (with POSIX macros)
 * 
 * Return Values: the function return 0 when the file is a regular file and
 * return -1 in all other cases.
 */
int is_directory(char * p_path) {

    struct stat * buf = malloc(sizeof(struct stat));

    if( ! stat(p_path, buf)) {
        if (S_ISDIR(buf->st_mode)) {
                free(buf);
                return _is_directory_ = 0;
        }
    } else {
        fprintf(stderr, "server: %s, %s\n", p_path, strerror(errno));
    }
    free(buf); 
    return -1;
}

/*
 * Checks a path
 * 
 * Checks if the path is a file, a dir or not exist. If the file doesn't 
 * exist, if is a dir or in all other case (for example you don't have the 
 * permissions to operate on it "rw") the function returns -1.
 *
 * Return value:
 *  0 on success
 * -1 on faliure
 */
int tools_path_checker(char * p_path) {

    if (is_file(p_path)) {
        fprintf(stderr, "server: The user-file doesn\'t exist!\n");
        if (tools_create_path(p_path)) 
        exit(EXIT_SUCCESS);
    }

    /*if (is_directory(p_path)) {
        if(tools_create_path(p_path)) exit(EXIT_SUCCESS);
    } else {
        fprintf(stderr, "You have selected a dir, not a file\n");
        exit(EXIT_FAILURE);
    }*/

    return 0;
}

/*
 *
 */
int tools_create_path (char * p_path) {

    printf("server: You want create a new user file? ");

    if( ! io_check_user_answer_str()) {
        if(-1 == creat(p_path, S_IREAD|S_IWRITE)) {
            fprintf(stderr, "%s\n", strerror(errno));
            return -1;
        } else {
            printf("server: User file created successfully\n");
            return 0;
        }
    } else {
        printf("server: I can\'t work without a user-file!\n");
        printf("server: Cannot continue, exiting now!\n");
        exit(EXIT_SUCCESS);
    }
}

/*
 * regcomp()  is  used to compile a regular expression into a form that is
 * suitable for subsequent regexec() searches.
 *
 * # See man regcomp && man regexec
 * 
 * Other useful tips are provided by IBM here:
 * #1 http://ibm.co/10JhvBe
 * #2 http://bit.ly/ZSgwmT
 *
 * ### ctype.h is not safe: isalnum(), isalpha() etc.. 
 * The header <ctype.h> declares several functions useful for classifying 
 * and mapping characters. representable as an unsigned char or shall
 * equal the value of the macro EOF. If the argument has any other value, 
 * the behavior is undeﬁned.
 *
 * found in irssi sources: common.h row 95
 * https://gist.github.com/anonymous/5604079
 *
 * stackoverflow.com discussion: http://bit.ly/12jeFFC
 *
 * ISO C99 -> pag. 181 ctype.h
 * http://cs.nyu.edu/courses/spring13/CSCI-GA.2110-001/downloads/C99.pdf
 */
int tools_regex_checker(char * the_string, const char * the_pattern) {

    int rc;
    regex_t preg;
    size_t nmatch = 2;
    /* 
     * The regmatch_t structure which is the type of pmatch is defined in
     * <regex.h>. pmatch must be dimensioned to have at least nmatch elements. 
     * These are filled in by regexec() with substring match addresses.
     * 
     * pmatch[N]: The rm_so != -1 indicates the start offset
     * 			  The rm_eo != -1 indicates the end offset
     */
    regmatch_t pmatch[2];

    if (0 != (rc = regcomp(&preg, the_pattern, REG_EXTENDED))) {
        fprintf(stderr, "regcomp() failed, returning nonzero (%d)\n", rc);
        exit(EXIT_FAILURE);
    }

    if (0 != (rc = regexec(&preg, the_string, nmatch, pmatch, 0))) {
        #ifdef VERBOSE
        DEBUG_PRINT("Failed to match '%s' with '%s',returning %d.\n", 
            the_string, the_pattern, rc);
        #endif
        return -1;
    } else {
        #ifdef VERBOSE
        // pmatch[0] = The entire regular expression's match addresses
        printf("With the whole expression, a matched substring \"%.*s\" "
            "is found at position %d to %d.\n",
            pmatch[0].rm_eo - pmatch[0].rm_so, &the_string[pmatch[0].rm_so],
            pmatch[0].rm_so, pmatch[0].rm_eo - 1);
        #endif
        return 0;
    }
}

/*
 *
 */
int tools_user_tokenizer (data_t * p_user, msg_t * p_msg) {

    // Necessary because the strtok function modify their first argument
    msg_t * p_tmp = malloc(sizeof(msg_t)); *p_tmp = *p_msg;

    if ( ! tools_regex_checker(p_msg->msg, REGISTRATION)) {
        strcpy(p_user->uname, strtok(p_tmp->msg, ":"));
        strcpy(p_user->name, strtok(NULL, ":"));
        strcpy(p_user->email, strtok(NULL, "\0"));
        free(p_tmp); p_tmp = NULL; return 0;
    }

    fprintf(stderr, "Failed to split msg string into tokens\n");
    free(p_tmp); p_tmp = NULL; return -1;
}    

/*
 * 
 */
int tools_cmd_tokenizer (cmd_t * p_cmd, msg_t * p_msg) {

    enum { SINGLE_ARG, DOUBLE_ARG, TRIPLE_ARG };

    // A commad with one argument (the command)
    char * single_cmd = "^#[a-z]{2,64}$";
    // A command with two arguments (the command & the text)
    char * double_cmd = "^#[a-z]{2,64} :[a-z]+$";
    // A command with three arguments (command, receiver, text)
    char * triple_cmd = "^#[a-z]{2,64} [a-z]+:[a-z]+$";
    // Necessary because the strtok function modify its first argument
    msg_t * p_tmp = malloc(sizeof(msg_t)); *p_tmp = *p_msg;

    if ( ! tools_regex_checker(p_msg->msg, CMD_PATTERN)) {
        if ( ! tools_regex_checker(p_msg->msg, single_cmd))
            p_cmd->type = SINGLE_ARG;
        else if ( ! tools_regex_checker(p_msg->msg, double_cmd))
            p_cmd->type = DOUBLE_ARG;
        else if ( ! tools_regex_checker(p_msg->msg, triple_cmd))
            p_cmd->type = TRIPLE_ARG;
        else {
            fprintf(stderr, "%s:%d: ", __FILE__, __LINE__);
            fprintf(stderr, "Error, unrecognized command. ");
            fprintf(stderr, "Tokenization failed\n");
            return -1;
        }
    }

    switch (p_cmd->type) {
        case SINGLE_ARG:
            strcpy(p_cmd->cmd, strtok(p_tmp->msg, "\n"));
            break;
        case DOUBLE_ARG:
            strcpy(p_cmd->cmd, strtok(p_tmp->msg, " :"));
	        strcpy(p_cmd->text, strtok(NULL, "\n"));
            break;
        case TRIPLE_ARG:
            strcpy(p_cmd->cmd, strtok(p_tmp->msg, " "));
            strcpy(p_cmd->dest, strtok(NULL, ":"));
	        strcpy(p_cmd->text, strtok(NULL, "\n"));
            break;
        default:
            fprintf(stderr, "%s:%d: ", __FILE__, __LINE__);
            fprintf(stderr, "Failed to split cmd string into tokens. ");
            fprintf(stderr, "Number of argument not recognized\n");
            return -1;
    }
    
    free(p_tmp); p_tmp = NULL; return 0;
}










