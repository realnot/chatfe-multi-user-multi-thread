#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#include "config.h"

// globals 
int * p_csd;
msg_t * p_msg;
data_t * p_user;

/*
 * Handle the request
 *
 * The procedure provide an easy way to handle each client request. 
 * The requests can be:
 *
 * 1) registration;  2) login;    3) logout;
 * 4) list;          5) single;   6) brdcast;
 *
 * Return value:
 *  0 on success
 * -1 on failure (Error during the parsing)
 */
int requests_handler (int * csd, msg_t * msg, data_t * user) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","requests_handler() reached");
    #endif
 
    // save the references and work globally
    p_csd = csd; p_msg = msg; p_user = user;

    // parse the client request
    if (requests_parser()) 
        return -1;

    switch (p_msg->type) {
        case MSG_REGLOG: get_registration(); break;
        case MSG_LOGIN: get_login(); break;
        case MSG_LOGOUT: get_logout(); break;
        case MSG_LIST: get_list(); break;
        case MSG_SINGLE:
        case MSG_BRDCAST: add_msg(); break;
    }

    return 0;
}

/*
 * Parse the request
 * 
 * Not much to say ... the function parse the type of request (message/command) 
 * and acts accordingly. The code is clear and explained below.
 *
 * Return value:
 *   0. On success
 *  -1. On failure 
 */
int requests_parser(void) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","requests_parser() reached");
    #endif

    char null[1024];
    const char * str_fail_1 = "Impossible to define the type of request!\n";
    const char * str_fail_2 = "Error, invalid message!\n";

    msg_t * p_tmp_msg = malloc(sizeof(msg_t)); *p_tmp_msg = *p_msg;

    // Commands accepted
    const char * p_list_cmd = "#ls";
    const char * p_dest_cmd = "#dest";
    const char * p_logout_cmd = "#logout";
    // A commad with one argument (the command)
    const char * p_single_arg = "^#[a-z]{2,64}$";
    // A command with two arguments (the command & the text)
    const char * p_double_arg = "^#[a-z]{2,64} :[a-z]+$";
    // A command with three arguments (command, receiver, text)
    const char * p_triple_arg = "^#[a-z]{2,64} [a-z]+:[a-z]+$";
    // A command should be in this way
    const char * p_cmd_pattern = "^#[a-z]{2,64}(( [a-z]+:[a-z]+)|( :[a-z]+))?";

    // ***** command requests here...
    if ( ! tools_regex_checker (p_msg->msg, p_cmd_pattern)) {
        // checks the type of command (single arg, double arg, triple arg).
        if ( ! tools_regex_checker (p_msg->msg, p_single_arg)) {
            if ( ! strcmp (p_msg->msg, p_list_cmd))
                p_msg->type = MSG_LIST;
            else if ( ! strcmp (p_msg->msg, p_logout_cmd))
                p_msg->type = MSG_LOGOUT;
        // double argument commands here... (like a MSG_BRDCAST)
        } else if ( ! tools_regex_checker (p_msg->msg, p_double_arg)) {
            if ( ! strncmp (p_msg->msg, p_dest_cmd, strlen(p_dest_cmd)))
                p_msg->type = MSG_BRDCAST;
        // triple argument commands here.. (like a MSG_SINGLE)
        } else if ( ! tools_regex_checker (p_msg->msg, p_triple_arg)) {
            if ( ! strncmp (p_msg->msg, p_dest_cmd, strlen(p_dest_cmd)))
                p_msg->type = MSG_SINGLE;
        }
    // ***** registration/login requests here...
    } else if (p_msg->type == MSG_REGLOG || p_msg->type == MSG_LOGIN) {
        // checks if the msg matches the "registration" pattern 
        if ( ! tools_regex_checker(p_msg->msg, REGISTRATION))
            p_msg->type = MSG_REGLOG;
        // checks if the msg matches the "login" pattern 
        else if ( ! tools_regex_checker(p_msg->msg, LOGIN))
            p_msg->type = MSG_LOGIN;

    // ***** all other undefinied requests here *****
    } else
        p_msg->type = UNDEFINIED;

    // ***** checks if the message type is set properly
    switch (p_msg->type) {
        case MSG_REGLOG: break;
        case MSG_LOGIN: break;
        case MSG_LOGOUT: break;
        case MSG_LIST: break;
        case MSG_SINGLE:
            // not consider the cmd, get the receiver, get the message
            strcpy(null, strtok(p_tmp_msg->msg, " "));
            strcpy(p_msg->receiver, strtok(NULL, ":"));
            strcpy(p_msg->msg, strtok(NULL, "\n"));
            if ( ! tools_regex_checker(p_msg->receiver, UNAME_PATTERN)) {
                if ( ! tools_regex_checker(p_msg->receiver, MSG_PATTERN))
                    break;
            }
            // Error, come back to the caller with str_fail_2 message
            p_msg->msg = malloc(strlen(str_fail_2) + 1);
            strcpy(p_msg->msg, str_fail_2);
            free(p_tmp_msg); 
            return -1;
        case MSG_BRDCAST:
            /* not consider the cmd, set receiver to "0" (that mean it's a
            broadcast) and get the message */
            strcpy(null, strtok(p_tmp_msg->msg, ":"));
            strcpy(p_msg->receiver, "0");
            strcpy(p_msg->msg, strtok(NULL, "\n"));
            if ( ! tools_regex_checker(p_msg->receiver, "0")) {
                if ( ! tools_regex_checker(p_msg->receiver, MSG_PATTERN))
                    break;
            }
            // Error, come back to the caller with str_fail_2 message
            p_msg->msg = malloc(strlen(str_fail_2) + 1);
            strcpy(p_msg->msg, str_fail_2);
            free(p_tmp_msg); 
            return -1;
        case UNDEFINIED:
        default:
            // Error, come back to the caller with str_fail_1 message
            p_msg->msg = malloc(strlen(str_fail_1) + 1);
            strcpy(p_msg->msg, str_fail_1);
            free(p_tmp_msg); 
            return -1;
    }

    free(p_tmp_msg); 
    return 0;
}

/*
 * Register a user
 *
 * This function provides a simple way to register a user, is composed of 4 
 * steps that are explained in detail below.
 *
 * Return value:
 *   0. On success
 *  -1. On failure 
 */
int get_registration(void) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","get_registration() reached");
    #endif

    memset(p_user, -1, sizeof(data_t));
    // tokenize the msg into respective fields (uname, name, email)
    if ( ! tools_user_tokenizer(p_user, p_msg))
        // save "user" to the user-file
        if ( ! io_write_data_t(p_user_file, p_user))
            // add user to hash table
            if( ! insert_hash(Table, *p_user, p_user->uname))
                // log the user and come back to the caller
                if ( ! get_login())
                    return 0;
    return -1;
};

/* 
 * Log-in a user
 *
 * The function checks if the user "p_user->uname" is inside the hash table 
 * and if it is, set to *p_csd the sockid end returns to the caller.
 *
 * Note: if the message is of type "MSG_LOGIN" the "username" is located in 
 * "p_msg->msg", otherwise the message is of type "MSG_REGLOG", has been
 * tokenized and it is located into p_user->uname.
 */
int get_login(void) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","get_login() reached");
    #endif

    const char * login_success = "You've been logged in!\n";
    const char * login_failure = "Error during the login: Wrong username!\n";
    const char * reglog_success = "Registration complited!\n";
    const char * reglog_failure = "Error during the registration!\n";
    const char * wrong_username = "Error, invalid username\n";

    node_t * p_node;

    if (p_msg->type == MSG_LOGIN) 
        strcpy (p_user->uname, p_msg->msg);

    // checks if the username match the "uname" pattern 
    if ( ! tools_regex_checker(p_user->uname, UNAME_PATTERN)) {
        if ( NULL != (p_node = search_hash (Table, p_user->uname))) {
            strcpy(p_user->uname, p_node->data.uname);
            strcpy(p_user->name, p_node->data.name);
            strcpy(p_user->email, p_node->data.email);
            p_user->sockid = *p_csd;
            // Add user into "p_user_online" list.
            if ( ! insert_list (p_user_online, *p_user)) {
                // set the right success message
                if (p_msg->type == MSG_LOGIN) {
                    p_msg->msg = malloc(strlen(login_success) + 1);
                    strcpy (p_msg->msg, login_success);
                } else {
                    p_msg->msg = malloc(strlen(reglog_success) + 1);
                    strcpy (p_msg->msg, reglog_success);
                }
                // everything ok, the current user (csd) is logged
                p_msg->type = LOGGED; return 0;
           }
        } else {
            // set the "login" failure message
            if (p_msg->type == MSG_LOGIN) {
                p_msg->msg = malloc(strlen(login_failure) + 1);
                strcpy (p_msg->msg, login_failure);
            // set the "reglog" failure message
            } else {
                p_msg->msg = malloc(strlen(reglog_failure) + 1);
                strcpy (p_msg->msg, reglog_failure);
            }
        }
    // Error, the "uname" pattern doesn't match
    } else {
        p_msg->msg = malloc(strlen(wrong_username) + 1);
        strcpy (p_msg->msg, wrong_username);
    }
    
    p_msg->type = NOT_LOGGED;
    return  -1;
}

/*
 * Log-out a user
 *
 * This function, using the procedure delete list, delete a user from a list 
 * (the list of connected users). 
 *
 * Return value:
 *   0. On success (deleted user)
 *  -1. On failure (if the user doesn't exists)
 */
int get_logout(void) {
    
    #ifdef DEBUG
        DEBUG_PRINT("%s\n","get_logout() reached");
    #endif

    const char * logout_success = "You have successfully logged out\n";
    const char * logout_failure = "Failed to log out\n";

    if ( ! delete_list (p_user_online, *p_user)) {
        p_msg->msg = malloc(strlen(logout_success) + 1);
        strcpy (p_msg->msg, logout_success);
        p_user_online->count--;
    } else {
        p_msg->msg = malloc(strlen(logout_failure) + 1);
        strcpy (p_msg->msg, logout_failure);
        return -1;
    }
    return 0;
}

/*
 * Get all connected users
 *
 * The function provides to client the list of users connected to the server. 
 * The first loop calculates the amount of memory required for the message and 
 * the second loop retrieves the users in the list "p_user_online".
 *
 * Return value: This function always succeeds,
 */
int get_list(void) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","get_list() reached");
    #endif
    
    node_t * p_list = p_user_online->head;
    char footer[32];
    const char * header = "\n[#] Users connected:";
    const char * body = "\n |--> ";
    sprintf(footer, "\n[#] The are %zu users online\n", p_user_online->count);

    size_t memory = 0;

    // get the number of bytes
    while(NULL != p_list) {
        memory += strlen(header) + 1; 
        memory += strlen(body) + 1;
        memory += strlen(footer) + 1;
        memory += strlen(p_list->data.uname) + 1;
        p_list = p_list->next;
    }
    // reserve memory for the list
    p_msg->msg = malloc(memory);

    p_list = p_user_online->head;
    // build the message "list".
    strcpy(p_msg->msg, header);
    while(NULL != p_list) {
        strcat(p_msg->msg, body);
        strcat(p_msg->msg, p_list->data.uname);
        p_list = p_list->next;
    }
    strcat(p_msg->msg, footer);

    return 0;
}

/*
 * Add a message to depot (the depot of requests)
 * 
 * Return value: This function always succeeds,
 */
int add_msg (void) {

    #ifdef DEBUG
        DEBUG_PRINT("%s\n","add_msg() reached");
    #endif

    size_t my_write_pos;

    // store the request to depot
    pthread_mutex_lock(&p_depot->mux);
    while ( p_depot->count == _MSGS_BUF_SIZE_ )
        pthread_cond_wait(&p_depot->full, &p_depot->mux);
    my_write_pos = p_depot->write_pos;
    p_depot->write_pos++;
    pthread_mutex_unlock(&p_depot->mux);

    // store the message
    p_depot->msgdep[my_write_pos] = *p_msg;
    p_depot->msgdep[my_write_pos].msg = malloc(sizeof(char *));
    strcpy(p_depot->msgdep[my_write_pos].msg, p_msg->msg);
    strcpy(p_depot->msgdep[my_write_pos].sender, p_user->uname);

    // update the depot
    pthread_mutex_lock(&p_depot->mux);
    p_depot->count++;
    if ( p_depot->write_pos >= _MSGS_BUF_SIZE_ )
        p_depot->write_pos = 0;
    pthread_cond_signal(&p_depot->empty);
    pthread_mutex_unlock(&p_depot->mux);

    return 0;
}
