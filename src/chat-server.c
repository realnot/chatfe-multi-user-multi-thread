#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#include "config.h"

#define MAXTHREADS 25
#define BACKLOG 25
#define PORT 1111

#define CMD_ERROR 1

void init_sequence(char **);

void * main_thread(void *);
void * worker_thread(void *);
void * dispatcher_thread (void *);
void sig_handler(int);
void shutdown_sck(int);
void print_usage(int, char *);

// Globals
int msd;
size_t go = 1;
size_t nthreads = 0;
list_t * p_connected_users;
pthread_mutex_t write_log;

int temp;

/*
 *
 */
int main(int argc, char ** argv) {
	
    switch (argc) {
        case 1: print_usage(1, argv[0]); break;
        case 2: print_usage(2, argv[1]); break;
        case 3: init_sequence(argv); break;
        default: exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}

/*
 *
 */
void init_sequence(char ** argv) {

    printf("Initializing starting sequence... ");
    pthread_t main_tid;
    // Create the main thread
    pthread_create(&main_tid, NULL, main_thread, (void *)argv);
    printf("Done!\n");
    // Wait the termination of the main thread
    pthread_join(main_tid, NULL);
}

/*
 * The main thread
 *
 * The main thread provides the initialization of the users table and then 
 * opens a socket master (AF_UNIX, AF_INET) on which the client programs can 
 * connect to access the service. The main thread performs a loop controlled 
 * by the global variable go, and at each loop. waits for a connection request 
 * from a user. For each connection request the main thread executes a worker 
 * thread in a detached modes.
 */
void * main_thread(void * args) {
	
    #ifdef DEBUG
            DEBUG_PRINT("main_thread() %zu reached!\n", pthread_self());
    #endif

    char ** argv = (char **)args;
    int ii = 0;
    size_t user_file_size;
    size_t log_file_size;
    pthread_attr_t attr;
    struct sockaddr_in sa;

    signal(SIGINT, sig_handler);
    signal(SIGCHLD, sig_handler);

    // Thread pool
    pthread_t worker_tid[MAXTHREADS];
    // Thread dispatcher
    pthread_t dispatcher_tid;

    // creates and initializes a depot of messages :)
    p_depot = malloc(sizeof(depot_t));
    pthread_mutex_init(&p_depot->mux, NULL);
    pthread_cond_init(&p_depot->full, NULL);
    pthread_cond_init(&p_depot->empty, NULL);
    p_depot->count = p_depot->read_pos = p_depot->write_pos = 0;

    // trace all connected users
    p_user_online = create_list();

    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(PORT);
    // htonl vs not htonl http://bit.ly/11fLqBj a good thread...
    /* A constant defined in the header files is INADDR_LOOPBACK, defined as 
    127.0.0.1, or 0x7F000001. This address is given in network-byte order, and 
    cannot be passed to the sockets interface without htonl(). 
    You must use htonl() with this constant. */
    sa.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    // The null char + strlen()
    user_file_size = 1 + strlen(argv[1]);
    log_file_size = 1 + strlen(argv[2]);

    // Make sure the path is not too long
    if (user_file_size > MAX_PATH_SIZE && log_file_size > MAX_PATH_SIZE) {
            fprintf(stderr, "Path size too big, 2048 bytes exceeded!\n");
            pthread_exit(NULL);	
    }
    // Allocate the right memory for caching the path string
    p_user_file = calloc (user_file_size, sizeof(char));
    p_log_file = calloc (log_file_size, sizeof(char));
    // Store the path
    strncpy(p_user_file, argv[1], user_file_size);
    strncpy(p_log_file, argv[2], log_file_size);

    // Creates hash table
    printf("Creating hash table... ");
    Table = create_hashtable();
    printf("Done\n");
    // Try to read the user-file and loads the data into hash table
    printf("Initializing hash table... ");
    if ( ! io_read_data_t(p_user_file))
            printf("Done!\n");

    // Create master socket
    msd = socket (AF_INET, SOCK_STREAM, 0);

    if ( -1 == msd) {
            perror("[#] Error while creating socket");
            close(msd); pthread_exit(NULL);
    }
    // Bind to tell sockets which port we want to serve.
    if ( -1 == bind ( msd, (struct sockaddr *)&sa, sizeof(sa) )) {
            perror("[#] Error on binding");
            close(msd); pthread_exit(NULL);
    }
    // Add the header to log fileu
    printf("Initializing log file...  ");
    if ( ! io_write_header_t(p_log_file))
            printf("Done!\n");

    // Create child process (like a BSD pro user :D )
    switch (fork()) {
        case -1:
            perror("[#] Error on fork");
            close(msd); pthread_exit(NULL);
            break;
        // father process
        default:
            exit(EXIT_SUCCESS);
            break;
        // child process
        case 0:
            break;
    }
    /* the backlog variable tells sockets how many incoming requests to accept 
    while you are busy processing the last request. In other words, it 
    determines the maximum size of the queue of pending connections. */
    if ( -1 == listen ( msd, BACKLOG)) {
            perror("[#] Error on listen");

    if ( pthread_create(&dispatcher_tid, NULL, dispatcher_thread, 0)) {
        fprintf(stderr, "[#] ERROR: failed to create thread\n");
        exit(EXIT_FAILURE);
    }

    pthread_attr_init(&attr);
    pthread_mutex_init(&write_log, NULL);
    /* pthread_join is a blocking call which is waiting for the thread 
    to finish. It means that you will not be able to accept any more 
    connections while that one thread is running. This is the 
    reason to use pthread_detach */
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    // starts to accept the requests
    while ( go ) {
        printf("Waiting for incoming connections on port %d\n", PORT);
        /* The accept returns a new socket. We will use this new socket to
        communicate with the client. The old socket "msd" continues to listen 
        for more requests "backlog". Now, the new socket is meant only for 
        communications.*/
        int * csd = malloc(sizeof(int));
        if ( -1 == (*csd = accept(msd, NULL, 0))) { 
            perror("[#] Error on accept");
            close(msd); pthread_exit(NULL);
        } else {
            printf("Received connection from %s\n", inet_ntoa(sa.sin_addr));
            // create a new thread to process the incomming request
            pthread_create(&worker_tid[ii++], &attr, worker_thread, csd);
            if (nthreads++ >= MAXTHREADS) {
                fprintf(stderr, "[#] Server overload, too many connections\n");
                break;
            }
        }
    }
    pthread_join(dispatcher_tid, NULL);
    pthread_mutex_destroy(&write_log);
    pthread_attr_destroy(&attr);
    close(msd); 

    free(p_connected_users);
    return NULL;
}

/*
 * The worker thread
 *
 * The worker thread executes a loop controlled by the global variable go, and 
 * for each loop: 
 *  - Waits for a request from the client.
 *  - Register the command in the log files and the access to the log file needs 
 *    to be done in mutual exclusion to avoid mixing prints with other threads.
 *  - If the client sent a command for registration or listing of the connected 
 *    users, it will be execute.
 *  - If the client has sent a request to send a message to a single user or 
 *    broadcast, it is passes it to the dispatcher thread. The interaction with 
 *    the dispatcher thread is according to the producer-consumer model, using 
 *    a circular buffer of size K;
 *
 * See the requests.c
 */
void * worker_thread (void * args) {

    #ifdef DEBUG
            DEBUG_PRINT("worker_thread() %zu reached!\n", pthread_self());
    #endif

    int * csd = (int *) args;
    msg_t * p_msg = NULL;
    data_t * p_user = malloc(sizeof(data_t)); 
    memset(p_user, '\0', sizeof(data_t));

    while ( go ) {
        // read a message from client
        p_msg = malloc(sizeof(msg_t));
        if (-1 != read (*csd, p_msg, sizeof(msg_t))) {
            p_msg->msg = malloc(p_msg->msglen);
            if (-1 != read (*csd, p_msg->msg, p_msg->msglen)) {
                if (strlen(p_msg->msg) == 0 ) 
                    break;
                // save the request to log-file
                if (0 != io_write_cmd_t(p_log_file, p_msg->msg)) {
                    fprintf(stderr, "[#] Error on io_write_cmd_t()\n");
                    exit(EXIT_FAILURE);
                }
                // handle the client request
                requests_handler(csd, p_msg, p_user);
                p_msg->msglen = strlen(p_msg->msg) + 1;
            } else { perror("[#]  Error on read");  pthread_exit(NULL); }
        } else { perror("[#]  Error on read"); pthread_exit(NULL); }

        // Only the dispatcher can handle the MSG_SINGLE/BRDCAST
        if (MSG_SINGLE != p_msg->type && MSG_BRDCAST != p_msg->type) {
                // write a message to client
                if (-1 == write (*csd, p_msg, sizeof(msg_t)) ||   
                        -1 == write (*csd, p_msg->msg, p_msg->msglen)) {
                        perror("[#] Error on write"); pthread_exit(NULL); 
                } 
                free(p_msg->msg); free(p_msg);
        }
    }

    // ends communication
    nthreads--; free(csd); free(p_msg); free(p_user);
    // Shutdown connection
    if ( -1 == shutdown (*csd, SHUT_RDWR))
        perror("[#] Can not shutdown socket");

    pthread_exit((void *) 0);
}

/*
 * The dispatcher thread - dispatch messages from a circular buffer
 *
 * The dispatcher thread executes a loop controlled by the global variable "go". 
 * At each loop extracts a request from the circular buffer, extracts from the 
 * request the name of the recipient, and through this string verifies that the 
 * user exists in the connected users list. If the user is logged in, gets its 
 * sockid and sends the message to recipient or recipients in case of broadcast.
 * If the user is not connected, checks that it is registered. To do that we 
 * need to access to the hash table in mutual exclusion to avoid concurrent 
 * access with the worker thread to elements that share the same hash key.
 */
void * dispatcher_thread (void * args) {

    #ifdef DEBUG
            DEBUG_PRINT("dispatcher_thread() %zu reached!\n", pthread_self());
    #endif

    int ii, csd, ret = -1;
    size_t my_read_pos;
    size_t qta_users = 0;
    msg_t * p_msg = NULL;
    socket_t * p_socket = malloc(sizeof(socket_t));

    while ( go ) {
        // Check the buffer
        pthread_mutex_lock(&p_depot->mux);
        while (p_depot->count == 0)
                pthread_cond_wait(&p_depot->empty, &p_depot->mux);
        my_read_pos = p_depot->read_pos;
        p_depot->read_pos++;
        pthread_mutex_unlock(&p_depot->mux);

        // Extract a request from circular buffer 
        p_msg = realloc(p_msg, sizeof(msg_t));
        *p_msg = p_depot->msgdep[my_read_pos];
        p_msg->msg = malloc(p_msg->msglen);
        strcpy(p_msg->msg, p_depot->msgdep[my_read_pos].msg);

        // Set qta_users for a single user
        if (MSG_SINGLE == p_msg->type)
                qta_users = 1;
        // Set qta_users for all online users (except the sender)
        else if (MSG_BRDCAST == p_msg->type)
                qta_users = p_user_online->count - 1;

        // Respectively: number of elements, size in bytes, the elements
        p_socket->len = qta_users;
        p_socket->size = qta_users * sizeof(int);
        p_socket->p_sockid = malloc(p_socket->size);

        // Get the sockid
        ret = get_sockid(p_socket, p_msg);

        // Send the message to a single user or all connected users
        for ( ii = 0; p_socket->len > ii; ii++) {
            csd = p_socket->p_sockid[ii];
            if (-1 == write (csd, p_msg, sizeof(msg_t)) ||
                -1 == write (csd, p_msg->msg, p_msg->msglen)) {    
                perror("[#] Error on write"); pthread_exit(NULL); 
            }
        }

        // Update the buffer
        pthread_mutex_lock(&p_depot->mux);
        p_depot->count--;
        if (p_depot->read_pos >= _MSGS_BUF_SIZE_)
                p_depot->read_pos = 0;
        pthread_cond_signal(&p_depot->full);
        pthread_mutex_unlock(&p_depot->mux);

        //free and continue
        free(p_socket->p_sockid); 
        free(p_msg->msg); 
        free(p_msg); 
        p_msg = NULL;
    }

    free(p_socket);
    return (void *) 0; 
}

/*
 * The signals handler
 *
 * The function handles some standard signals
 *
 * Return value:
 *  None.
 */
void sig_handler(int signo) {

    // Interrupt
    if (signo == SIGINT) {
        printf("received signal SIGINT (ctrl+c)\n");
        shutdown_sck(0);
    }
    /* Signal sent to parent process whenever one of its child 
    processes terminates or stops. */
    else if (signo == SIGCHLD) {
        printf("received signal SIGCHLD\n");
    }
}

/* 
 * The shutdown
 *
 * The function terminates the socket
 *
 * Return value:
 *  None
 */
void shutdown_sck(int exit_value) {

    go = 0;

    printf("Initializing shutdown sequence... ");
    if ( -1 == shutdown (msd, SHUT_RDWR))
        perror("[#] Can not shutdown socket");

    // end connection
    //free(p_msg->msg); 
    //free(p_msg);
    close(msd);

    if (-1 == exit_value)
        fprintf(stderr, "An error occurred!!!\n");
    else
        printf("Done\n");
    printf("Exiting now...\n");

    -1 == exit_value ? exit(EXIT_FAILURE) : exit(EXIT_SUCCESS);
}

/*
 * This function just provide the information for the correct usage.
 */
void print_usage(int argc, char * argv) {

    printf("Usage: chat-server [user-file] [log-file]\n");

    switch (argc) {
        case 1: printf("[%s] Where are the options?\n", argv); break;
        case 2: printf("[%s] One parameter missing!\n", argv); break;
        default: exit(EXIT_FAILURE);
    }
}
